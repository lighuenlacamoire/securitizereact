export interface MessageTitle {
  title?: string;
  message?: string;
}

export type ValidationError = {
  field: string;
  message: string;
  validation: string;
};

export interface FormError {
  [key: string]: string | undefined;
}

export interface GenericItem {
  id: string; // id milisegundos
  name: string;
  color?: string;
  disabled?: boolean;
}
