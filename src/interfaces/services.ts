export interface IMainMethods<T> {
  data?: T;
  error?: IError;
}

export interface IError {
  statusCode: number;
  statusText: string;
}

export interface Header {
  [key: string]: string;
}
export type HttpResponse = {
  code?: string | number;
  status?: string | number;
  message?: string;
  user?: string;
};

export interface ErrorDetail {
  domain: string;
  reason: string;
  message: string;
  locationType: string;
  location: string;
}
export interface AppError {
  errors?: ErrorDetail[];
  code?: string | number;
  status?: string | number;
  message?: string;
  title?: string;
}

export interface ApiHttpError {
  error: AppError;
}
