/**
 * Setea el/los elementos de la lista segun este activado o no
 * @param {Array} data Lista de elementos
 * @param {Function} setData Funcion para actualizar la lista
 * @param {boolean} multiSelect Indica si puede haber mas de un seleccionado
 * @param {number} index Index del item en la lista
 */
const setListActive = <TEntity>(
  data: (TEntity & { active: boolean })[],
  setData: (data: (TEntity & { active: boolean })[]) => void,
  multiSelect: boolean,
  index: number,
): void => {
  const mod = data[index];
  let newArr = [...data];
  if (!multiSelect && mod.active === false) {
    newArr = newArr.map((x) => {
      return { ...x, active: false };
    });
    mod.active = true;
  } else if (multiSelect) mod.active = !mod.active;

  newArr[index] = mod;
  setData(newArr);
};

const wait = async (ms: number): Promise<unknown> => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

export { setListActive, wait };
