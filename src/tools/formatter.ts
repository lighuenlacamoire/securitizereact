const validateEmail = (value: string): boolean =>
  value.match(
    /^(([^<>()[\]\\.,;:\s@\\"]+(\.[^<>()[\]\\.,;:\s@\\"]+)*)|(\\".+\\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  ) != null;

const validateDate = (value: string): boolean => {
  const length = value.length;
  let isValid = true;
  let reason = "";
  if (length == 10) {
    const inputText = value;
    const dtArray = inputText.split("/");
    const dd = toNumber(dtArray[0]);
    const mm = toNumber(dtArray[1]);
    const yy = toNumber(dtArray[2]);

    const ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (mm == 1 || (mm > 2 && mm <= 12)) {
      if (dd > ListofDays[mm - 1]) {
        isValid = false;
        reason = "Invalid date";
      }

      const yearnow = new Date().getFullYear();
      if (yy < 1900 || yy > yearnow) {
        isValid = false;
        reason = "Invalid year date";
      }
    } else if (mm == 2) {
      let leapyear = false;
      if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
        leapyear = true;
      }
      if (!leapyear && dd >= 29) {
        isValid = false;
        reason = "Not a leap year";
      }
      if (leapyear && dd > 29) {
        isValid = false;
        reason = "Invalid leap year date";
      }
    } else {
      isValid = false;
      reason = "Invalid month";
    }
  } else {
    isValid = false;
    reason = "Invalid format";
  }

  return isValid;
};

const toNumber = (value: string): number => parseInt(value || "0", 10);
/**
 * Restringe que el valor enviado sea tipo Numerico
 * @param {string} value valor que debe ser Numerico
 */
const restrictNumber = (value: string): string => value.replace(/[^0-9]/gm, "");

const restrictDateCheck = (str: string, max: number): string => {
  if (str.charAt(0) !== "0" || str == "00") {
    let num = parseInt(str);
    if (isNaN(num) || num <= 0 || num > max) num = 1;
    str =
      num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
        ? "0" + num
        : num.toString();
  }
  return str;
};

const restrictDate = (value: string) => {
  let input = value;
  if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
  const values = input.split("/").map(function (v) {
    return v.replace(/\D/g, "");
  });
  if (values[0]) values[0] = restrictDateCheck(values[0], 12);
  if (values[1]) values[1] = restrictDateCheck(values[1], 31);
  const output = values.map((v, i) => {
    return v.length == 2 && i < 2 ? v + " / " : v;
  });
  return output.join("").substr(0, 14);
};

const restrictDate3 = (value: string) => {
  /**
   * leap year
   (((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))
   
   /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([1][26]|[2468][048]|[3579][26])00))))$/g
  
   */

  const pattern = new RegExp(
    /^(0?[1-9]|[12][0-9]|3[01])[/\-.](0?[1-9]|1[012])[/\-.](\d{4})$/,
  );
  //new RegExp(/^((0[1-9]|[12][0-9]|3[01])(\/)(0[13578]|1[02]))|((0[1-9]|[12][0-9])(\/)(02))|((0[1-9]|[12][0-9]|3[0])(\/)(0[469]|11))(\/)\d{4}$/)
  const match = pattern.exec(value);
  if (match == null) return false;

  //var year = match[1];
  //var month = match[2]*1;
  //var day = match[3]*1;
  const year = toNumber(match[3]);
  const month = toNumber(match[2]) * 1;
  const day = toNumber(match[1]) * 1;
  const date = new Date(year, month - 1, day); // because months starts from 0.

  return (
    date.getFullYear() == year &&
    date.getMonth() == month - 1 &&
    date.getDate() == day
  );
};

const restrictDate2 = (value: string): string =>
  value.replace(
    /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[1,2])\/(19|20|21)\d{2}$/gm,
    "$1",
  );
const restrictMask = (x: string, pattern: string, mask: string) => {
  const strippedValue = x.replace(/[^0-9]/g, "");
  const chars = strippedValue.split("");
  let count = 0;

  let formatted = "";
  for (let i = 0; i < pattern.length; i++) {
    const c = pattern[i];
    if (chars[count]) {
      if (/\*/.test(c)) {
        formatted += chars[count];
        count++;
      } else {
        formatted += c;
      }
    } else if (mask) {
      if (mask.split("")[i]) formatted += mask.split("")[i];
    }
  }
  return formatted;
};

export {
  toNumber,
  validateEmail,
  restrictNumber,
  restrictMask,
  restrictDate,
  validateDate,
};
