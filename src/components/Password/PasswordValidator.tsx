import { CheckBox } from "components/Check";
import React, { useEffect, useState } from "react";

const ownContent = {
  validateLength: {
    text: "Be a minimum of 8 characters",
    key: "passLength",
    regex: /^.{8,}$/,
  },
  validateLower: {
    text: "Include at least one lowercase (a-z)",
    key: "passLower",
    regex: /^(?=.*[a-z]).+$/,
  },
  validateUpper: {
    text: "Include at least one uppercase (A-Z)",
    key: "passUpper",
    regex: /^(?=.*[A-Z]).+$/,
  },
  validateNumber: {
    text: "Include at least one number (0-9)",
    key: "passNumber",
    regex: /^(?=.*[0-9]).+$/,
  },
};

type PasswordChecks = {
  [key: string]: boolean;
};

type Props = {
  value: string;
  onValidate:
    | (<T>(a: T) => T | Promise<T>)
    | (() => void)
    | ((value: boolean) => void)
    | (() => Promise<void>);
};

const PasswordValidator = ({ value, onValidate }: Partial<Props>) => {
  const [passChecks, setPassChecks] = useState<PasswordChecks>({
    [ownContent.validateLength.key]: false,
    [ownContent.validateLower.key]: false,
    [ownContent.validateUpper.key]: false,
    [ownContent.validateNumber.key]: false,
  });

  const verifyValidations = () => {
    let passed = true;
    Object.keys(passChecks).forEach((key, index) => {
      if (passChecks[key] === false) {
        passed = false;
      }
    });
    if (onValidate) {
      onValidate(passed);
    }
  };

  const applyValidations = () => {
    const checkLength = ownContent.validateLength.regex.test(value ?? "");
    const checkLower = ownContent.validateLower.regex.test(value ?? "");
    const checkUpper = ownContent.validateUpper.regex.test(value ?? "");
    const checkNumber = ownContent.validateNumber.regex.test(value ?? "");
    setPassChecks({
      ...passChecks,
      [ownContent.validateLength.key]: checkLength,
      [ownContent.validateLower.key]: checkLower,
      [ownContent.validateUpper.key]: checkUpper,
      [ownContent.validateNumber.key]: checkNumber,
    });
  };

  useEffect(() => {
    applyValidations();
  }, [value]);

  useEffect(() => {
    verifyValidations();
  }, [passChecks]);

  return (
    <div>
      <div className="Text-Small" style={{ color: "var(--color-text)" }}>
        <label>Passwords must:</label>
      </div>
      <div className="Form-Container-Radio">
        <CheckBox
          id={`chk${ownContent.validateLength.key}`}
          isRadio
          checked={passChecks.passLength}
          style={{ marginRight: 8 }}
        />
        <label>{ownContent.validateLength.text}</label>
      </div>
      <div className="Form-Container-Radio">
        <CheckBox
          id={`chk${ownContent.validateLower.key}`}
          isRadio
          checked={passChecks.passLower}
          style={{ marginRight: 8 }}
        />
        <label>{ownContent.validateLower.text}</label>
      </div>
      <div className="Form-Container-Radio">
        <CheckBox
          id={`chk${ownContent.validateUpper.key}`}
          isRadio
          checked={passChecks.passUpper}
          style={{ marginRight: 8 }}
        />
        <label>{ownContent.validateUpper.text}</label>
      </div>
      <div className="Form-Container-Radio">
        <CheckBox
          id={`chk${ownContent.validateNumber.key}`}
          isRadio
          checked={passChecks.passNumber}
          style={{ marginRight: 8 }}
        />
        <label>{ownContent.validateNumber.text}</label>
      </div>
    </div>
  );
};

export default PasswordValidator;
