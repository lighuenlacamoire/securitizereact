import React from "react";

const LogoForm = () => {
  return (
    <div className="Logo-Container-Form">
      <img className="Logo-Container-Img" />
      <img className="Logo-Container-Title" />
    </div>
  );
};

export default LogoForm;
