import { KeyboardTypes } from "configuration/constants";
import React, { CSSProperties, ChangeEvent } from "react";
import { restrictNumber } from "tools/formatter";

type Props = {
  id: string;
  legend: string;
  value: string;
  error: string;
  keyboard: KeyboardTypes;
  containerStyle: CSSProperties;
  onValueChange: (value: string) => void;
};

/**
 * Component de Input generico para los formularios
 * @param {string} id Id del input y enlace con el label
 * @param {string} legend
 * @param {string} value
 * @param {string} error
 * @param {KeyboardTypes} keyboard
 * @param {CSSProperties} containerStyle
 * @param {Function} onValueChange
 */
const InputText = (props: Partial<Props>) => {
  const { id, value, legend, error, keyboard, containerStyle, onValueChange } =
    props;

  const handleChange = (value: string) => {
    if (onValueChange) {
      switch (keyboard) {
        case KeyboardTypes.NUMERIC: {
          onValueChange(restrictNumber(value));
          break;
        }
        case KeyboardTypes.DATE: {
          const newDate = value
            .replace(/^(\d\d)(\d)$/g, "$1/$2")
            .replace(/^(\d\d\/\d\d)(\d+)$/g, "$1/$2")
            .replace(/[^\d/]/g, "");
          onValueChange(newDate);
          break;
        }
        case KeyboardTypes.TEXT: {
          onValueChange(value);
          break;
        }
        default:
          onValueChange(value);
          break;
      }
      /*
      onValueChange(
        isNumeric ? restrictNumber(event.target.value) : event.target.value,
      );
      //onValueChange(event.target.value);
      */
    }
  };
  return (
    <div
      className={`Form-Field-Container ${error ? "error" : ""}`}
      style={containerStyle}>
      <input
        type="text"
        {...(id ? { id } : {})}
        placeholder="|"
        value={value}
        onChange={(event: ChangeEvent<HTMLInputElement>) => {
          handleChange(event.target.value);
        }}
      />
      {legend ? <label {...(id ? { htmlFor: id } : {})}>{legend}</label> : null}
    </div>
  );
};

export default InputText;
