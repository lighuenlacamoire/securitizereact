import React, {
  useState,
  useRef,
  useEffect,
  RefObject,
  Dispatch,
  SetStateAction,
  MutableRefObject,
  ChangeEvent,
  KeyboardEvent,
  CSSProperties,
} from "react";
import { restrictNumber } from "tools/formatter";

const inputStyle: CSSProperties = { width: 56, textAlign: "center" };

type Value = string | undefined;

type Change = Dispatch<SetStateAction<Value>>;

type Reference = MutableRefObject<HTMLInputElement | undefined>;

type Props = {
  code: string | undefined;
  error: string | undefined;
  containerStyle?: CSSProperties;
  onValueChange: (text: string) => void | undefined;
};

/**
 * Componente de ingreso de codigo por digito
 * @param code Codigo completo
 * @param error
 * @param {Function} onValueChange Funcion para actualizar el valor completo
 * @returns {JSX.Element}
 */
const InputCoding = (props: Props): JSX.Element => {
  const { code, onValueChange, containerStyle, error } = props;

  const [formInputOne, setFormInputOne] = useState<Value>(
    code && code.length > 0 ? code.slice(0, 1) : "",
  );
  const [formInputTwo, setFormInputTwo] = useState<Value>(
    code && code.length > 1 ? code.slice(1, 2) : "",
  );
  const [formInputThree, setFormInputThree] = useState<Value>(
    code && code.length > 2 ? code.slice(2, 3) : "",
  );
  const [formInputFour, setFormInputFour] = useState<Value>(
    code && code.length > 3 ? code.slice(3, 4) : "",
  );
  const [formInputFive, setFormInputFive] = useState<Value>(
    code && code.length > 4 ? code.slice(4, 5) : "",
  );
  const [formInputSix, setFormInputSix] = useState<Value>(
    code && code.length > 5 ? code.slice(5, 6) : "",
  );

  const refInputOne = useRef<HTMLInputElement>();
  const refInputTwo = useRef<HTMLInputElement>();
  const refInputThree = useRef<HTMLInputElement>();
  const refInputFour = useRef<HTMLInputElement>();
  const refInputFive = useRef<HTMLInputElement>();
  const refInputSix = useRef<HTMLInputElement>();

  const hasError = error && error.length > 0;

  /**
   * Actualiza el valor del input y pasa al proximo
   * @param {string | undefined} value Valor brindado por el input
   * @param {Dispatch<SetStateAction<string | undefined>>} change Funcion para actualizar el valor brindado
   * @param {MutableRefObject<HTMLInputElement | undefined>} reference referencia del siguiente input
   */
  const addNumber = (value: Value, change: Change, reference: Reference) => {
    const newValue = restrictNumber(value || "");
    change(newValue);

    if (newValue && newValue?.length > 0) {
      reference.current?.focus();
    }
  };

  /**
   * Chequea si fue presionada la tecla de borrado
   * @param {string} keyPressed tecla presionada
   * @param {MutableRefObject<HTMLInputElement | undefined>} reference referencia del siguiente input
   */
  const lookBackSpace = (keyPressed: string, reference: Reference) => {
    if (keyPressed === "Backspace") {
      reference.current?.focus();
    }
  };

  /**
   * Actualiza el codigo completo
   */
  const changeCode = () => {
    const newCode = `${formInputOne}${formInputTwo}${formInputThree}${formInputFour}${formInputFive}${formInputSix}`;
    onValueChange(newCode);
  };

  useEffect(() => {
    changeCode();
  }, [
    formInputOne,
    formInputTwo,
    formInputThree,
    formInputFour,
    formInputFive,
    formInputSix,
  ]);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        ...containerStyle,
      }}>
      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputOne}
          ref={refInputOne as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputOne, refInputTwo);
          }}
          type="text"
        />
      </div>

      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputTwo}
          ref={refInputTwo as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputTwo, refInputThree);
          }}
          type="text"
          onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
            lookBackSpace(event.key, refInputOne);
          }}
        />
      </div>

      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputThree}
          ref={refInputThree as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputThree, refInputFour);
          }}
          type="text"
          onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
            lookBackSpace(event.key, refInputTwo);
          }}
        />
      </div>

      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputFour}
          ref={refInputFour as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputFour, refInputFive);
          }}
          type="text"
          onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
            lookBackSpace(event.key, refInputThree);
          }}
        />
      </div>

      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputFive}
          ref={refInputFive as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputFive, refInputSix);
          }}
          type="text"
          onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
            lookBackSpace(event.key, refInputFour);
          }}
        />
      </div>
      <div className={`Form-Field-Container`}>
        <input
          style={inputStyle}
          value={formInputSix}
          ref={refInputSix as RefObject<HTMLInputElement>}
          maxLength={1}
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            addNumber(event.target.value, setFormInputSix, refInputSix);
          }}
          type="text"
          onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
            lookBackSpace(event.key, refInputFive);
          }}
        />
      </div>
    </div>
  );
};

export default InputCoding;
