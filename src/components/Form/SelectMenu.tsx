import { IconArrowDown } from "components/Icon/SVG";
import { OptionProp } from "interfaces/buttons";
import React, { useState } from "react";
import { setListActive } from "tools/functions";

type Props = {
  containerStyle: React.CSSProperties;
  options: OptionProp[];
  onOptionsChange: (options: OptionProp[]) => void;
  error: string;
};

const SelectMenu = (props: Partial<Props>) => {
  const { containerStyle, options, onOptionsChange, error } = props;

  const selected = options?.find((item) => item.active);

  const [expanded, setExpanded] = useState(false);

  const toggleListItem = () => {
    setExpanded(!expanded);
  };
  return (
    <div
      style={containerStyle}
      className={`Form-Field-Container ${expanded ? "active" : ""}`}>
      <div className="select-btn" onClick={toggleListItem}>
        <span className="sBtn-text">{selected?.name}</span>
        <IconArrowDown />
      </div>

      <ul
        className="options"
        style={
          containerStyle && containerStyle.maxWidth
            ? { maxWidth: containerStyle.maxWidth }
            : {}
        }>
        {options?.map((item, index) => (
          <li
            key={`${index}${item.id}`}
            className="option"
            onClick={() => {
              if (onOptionsChange) {
                setListActive(options ?? [], onOptionsChange, false, index);
              }
              toggleListItem();
            }}>
            <span className="option-text">{item.name}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SelectMenu;
