import { CheckBox } from "components/Check";
import React from "react";

type Props = {
  id: string;
  text: string;
  checked: boolean;
  onPress:
    | ((e: React.FormEvent<HTMLDivElement>) => void)
    | ((e: React.FormEvent<HTMLDivElement>) => Promise<void>)
    | (<T>(a: T) => T | Promise<T>)
    | (() => void)
    | (() => Promise<void>);
};

const InputCheck = ({ id, text, checked, onPress }: Props) => {
  return (
    <div className="Form-Container-Check" onClick={onPress}>
      <CheckBox id={id} checked={checked} style={{ marginRight: 8 }} />
      <label htmlFor={id}>{text}</label>
    </div>
  );
  /*
  return (
    <div className="Form-Container-Check2">
      <input id={id} type="checkbox" onChange={onPress} checked={checked} />
      <label htmlFor={id}>
        <span></span>
        <ins>{text}</ins>
      </label>
    </div>
  );
  */
};

export default InputCheck;
