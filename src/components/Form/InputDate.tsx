import React, { ChangeEvent } from "react";
import { restrictDate } from "tools/formatter";

type Props = {
  id: string;
  value: string;
  onValueChange: (value: string) => void;
};

const InputDate = (props: Props) => {
  const { id, value, onValueChange } = props;
  return (
    <input
      type="text"
      id={id}
      placeholder="|"
      autoComplete="off"
      maxLength={10}
      value={value}
      onChange={(event: ChangeEvent<HTMLInputElement>) => {
        const nose = restrictDate(event.target.value);
        console.log(nose);
        const newDate = event.target.value
          .replace(/^(\d\d)(\d)$/g, "$1/$2")
          .replace(/^(\d\d\/\d\d)(\d+)$/g, "$1/$2")
          .replace(/[^\d/]/g, "");
        onValueChange(newDate);
      }}
    />
  );
};
export default InputDate;
