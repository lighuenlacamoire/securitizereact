import React, { CSSProperties } from "react";

type Props = {
  checked: boolean;
  isRadio?: boolean;
  id?: string;
  onPress?:
    | ((e: React.FormEvent<HTMLSpanElement>) => void)
    | ((e: React.FormEvent<HTMLSpanElement>) => Promise<void>)
    | (<T>(a: T) => T | Promise<T>)
    | (() => void)
    | (() => Promise<void>);
  style?: CSSProperties;
};

const CheckBox = ({ checked, isRadio, id, onPress, style }: Props) => {
  return (
    <div style={{ position: "relative" }}>
      <span
        {...(id ? { id: id } : {})}
        {...(onPress ? { onClick: onPress } : {})}
        className={`${isRadio ? "Form-Field-Radio" : "Form-Field-Check"} ${
          checked ? "Checked" : ""
        }`}
        style={style}
      />
    </div>
  );
};

export default CheckBox;
