import { LoaderSmall } from "components/Loader";
import { ImageProps } from "interfaces/buttons";
import React, { useEffect, useRef, useState } from "react";

type Props = {
  containerStyle: React.CSSProperties;
  content: JSX.Element | string;
  subtitle: string;
  icon: Partial<ImageProps>;
  inverse: boolean;
  disabled: boolean;
  loading: boolean;
  onPress:
    | ((e: React.FormEvent<HTMLButtonElement>) => void)
    | ((e: React.FormEvent<HTMLButtonElement>) => Promise<void>)
    | (<T>(a: T) => T | Promise<T>)
    | (() => void)
    | (() => Promise<void>);
  color: string;
  testID?: string;
};

/**
 * Componente de Boton Pildora
 * @param {React.CSSProperties} containerStyle Estilo para el boton
 * @param {JSX.Element | string} content Texto o contenido de tipo <Text />
 * @param {string} subtitle Texto debajo del principal
 * @param {ImageProps} icon Label del icono o Componente <Icon />
 * @param {boolean} inverse Indica si es tipo inverso
 * @param {boolean} disabled Indica si esta deshabilitado
 * @param {Function} onPress Funcion al presionar el boton
 * @param {string} color Color principal para aplicar
 * @param {string} testID
 */
const ButtonSquare = (props: Partial<Props>) => {
  const {
    content,
    subtitle,
    icon,
    inverse,
    disabled,
    loading,
    color,
    containerStyle,
    testID,
    onPress,
  } = props;

  return (
    <button
      className="Form-Button-Default"
      onClick={onPress}
      disabled={disabled}
      style={{
        ...containerStyle,
      }}>
      {loading ? <LoaderSmall /> : <span>{content}</span>}
    </button>
  );
};

export default ButtonSquare;
