import React from "react";

interface Props {
  currentPage?: string;
  children?: React.ReactNode;
}

const PageContainer = ({ currentPage, children }: Props) => {
  return (
    <div className="Main">
      <div style={{ flex: 1, display: "flex", zIndex: 1, overflowX: "hidden" }}>
        {children}
      </div>
    </div>
  );
};

export default PageContainer;
