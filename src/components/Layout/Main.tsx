import { Pages } from "configuration/constants";
import React, { useState } from "react";
import { NavLink, BrowserRouter } from "react-router-dom";
import { getItem } from "tools/storage";

interface MainProps {
  children?: React.ReactNode;
}

const Main = ({ children }: MainProps): JSX.Element => {
  const accessToken = getItem("@accessToken");
  const handleLink = (navData: { isActive: boolean }) => {
    return navData.isActive ? "active" : "";
  };

  return (
    <BrowserRouter basename={process.env.PUBLIC_URL || Pages.HOMEPAGE}>
      <div className="Body">
        {accessToken ? (
          <div className={`Sidebar open`}>
            <NavLink to={Pages.HOMEPAGE} className={handleLink}>
              <span>hogar</span>
            </NavLink>
          </div>
        ) : null}
        {children}
      </div>
    </BrowserRouter>
  );
};

export default Main;
