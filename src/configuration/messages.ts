const genericMessages = {
  ok: "Ok",
  next: "Next",
  add: "Add",
  createAccount: "Create Account",
};

const homeMessages = {
  header: "Home",
  title: "Welcome",
  form: {
    wallet: {
      title: "Wallet",
    },
  },
};

const loginMessages = {
  header: "Log in",
  title: "Log in your account",
  form: {
    username: {
      title: "Usuario",
    },
    password: {
      title: "Password",
    },
  },
};

export { genericMessages, loginMessages, homeMessages };
