/** Rutas de navegacion de la App */
enum Pages {
  LOGINPAGE = "/Login",
  HOMEPAGE = "/",
}

enum KeyboardTypes {
  NUMERIC = "Numeric",
  DATE = "Date",
  TEXT = "Text",
  PASSWORD = "Password",
}

export { Pages, KeyboardTypes };
