import { Pages } from "configuration/constants";
import React from "react";
import { useNavigate, useLocation, Navigate, Outlet } from "react-router-dom";
import { getItem } from "tools/storage";

const PrivateRouter = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const accessToken = getItem("@accessToken");
  return accessToken ? <Outlet /> : <Navigate to={Pages.LOGINPAGE} replace />;
};

export default PrivateRouter;
