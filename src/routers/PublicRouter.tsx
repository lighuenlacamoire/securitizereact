import { Pages } from "configuration/constants";
import React from "react";
import { useNavigate, useLocation, Navigate, Outlet } from "react-router-dom";
import { getItem } from "tools/storage";

const PublicRouter = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const accessToken = getItem("@accessToken");
  return accessToken ? <Navigate to={Pages.HOMEPAGE} replace /> : <Outlet />;
};

export default PublicRouter;
