import { Pages } from "configuration/constants";
import { NotFoundPage } from "pages/Error";
import { HomePage } from "pages/Home";
import { LoginPage } from "pages/Login";
import React from "react";
import { Route, Routes } from "react-router-dom";
import PrivateRouter from "./PrivateRouter";
import PublicRouter from "./PublicRouter";

const RootRouter = () => {
  return (
    <Routes>
      <Route element={<PublicRouter />}>
        <Route path={Pages.LOGINPAGE} element={<LoginPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
      <Route element={<PrivateRouter />}>
        <Route path={Pages.HOMEPAGE} element={<HomePage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
    </Routes>
  );
};

export default RootRouter;
