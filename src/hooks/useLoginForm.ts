import { FormError } from "interfaces/configuration";
import { LoginRequestDTO } from "interfaces/models";
import { useState } from "react";

export interface LoginFormError {
  username?: string;
  password?: string;
}

const useLoginForm = () => {
  const [formValues, setFormValues] = useState<LoginRequestDTO>({
    username: "",
    password: "",
  });
  /** Form: errores */
  const [formErrors, setFormErrors] = useState<LoginFormError>();

  const handleChange = (key: keyof LoginRequestDTO, value: string) => {
    setFormValues((prevState) => {
      return {
        ...prevState,
        [key]: value,
      };
    });
  };

  const handleErrors = (key: keyof LoginFormError, value: string) => {
    setFormErrors((prevState) => {
      return {
        ...prevState,
        [key]: value,
      };
    });
  };

  const submitErrors = (data: LoginFormError) => {
    setFormErrors(data);
  };

  return { formValues, handleChange, formErrors, handleErrors, submitErrors };
};

export default useLoginForm;
