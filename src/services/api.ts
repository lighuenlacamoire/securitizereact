/* eslint-disable no-useless-catch */
import axios, { AxiosRequestConfig, Method, isAxiosError } from "axios";
import { Header } from "../interfaces/services";
import { deleteItem, getItem } from "tools/storage";
import { Pages } from "configuration/constants";

const config = {
  baseURL: process.env.REACT_APP_BACKENDURL,
};

export const axiosInstance = axios.create(config);

/**
 * API Calls Handler
 * @param {string} endpoint Endpoint path
 * @param {string} method HTTP Method
 * @param {Header?} header Headers si los necesitase
 * @param {unknown?} body Request body
 * @param {unknown?} parameters Query params
 * @returns {T} respuesta del servicio
 */
export const ApiCall = async <T>(
  endpoint: string,
  method: string,
  header?: Header,
  body?: unknown,
  parameters?: unknown,
): Promise<T> => {
  try {
    const formHeaders = setRequestHeaders(header);
    const requestConfig: AxiosRequestConfig = {
      method: method as Method,
      url: endpoint,
      data: body,
      params: parameters,
      headers: formHeaders,
    };
    console.log("Service", requestConfig.url);
    const response = await axiosInstance.request<T>(requestConfig);
    return response?.data;
  } catch (err) {
    if (isAxiosError(err)) {
      if (err.response?.status === 401) {
        deleteItem("@accessToken");
        window.location.href = `http://localhost:5000${Pages.LOGINPAGE}`;
      }
    }
    console.log("service er", err);
    throw err;
  }
};

/**
 * Crea los cabezales para el request HTTP
 * @param {Header} headers Headers en caso de ser necesario
 */
export const setRequestHeaders = (header?: Header): Header => {
  const token = getItem("@accessToken");
  const formHeaders = header || headers;

  if (token && !header) {
    return {
      ...formHeaders,
      Authorization: `Bearer ${token}`,
    };
  }

  return formHeaders;
};

export const headers: Header = {
  Accept: "application/json",
  "Content-Type": "application/json",
};
