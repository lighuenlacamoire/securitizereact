import { BalanceDTO, TransactionDTO } from "interfaces/models";
import { ApiCall } from "services/api";

const controllerName = "Address";

const GetEtherBalanceByAddress = async (address: string) =>
  ApiCall<BalanceDTO>(`${controllerName}/${address}`, "GET");

const GetAllTransactions = async (address: string) =>
  ApiCall<TransactionDTO[]>(`${controllerName}/${address}/Transactions`, "GET");

export { GetEtherBalanceByAddress, GetAllTransactions };
