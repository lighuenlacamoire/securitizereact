import { LoginRequestDTO, LoginResponseDTO } from "interfaces/models";
import { ApiCall } from "services/api";
import { saveItem } from "tools/storage";

const controllerName = "Account";

const Login = async (model: LoginRequestDTO) =>
  ApiCall<LoginResponseDTO>(
    `${controllerName}/Login`,
    "POST",
    undefined,
    model,
  );

export { Login };
