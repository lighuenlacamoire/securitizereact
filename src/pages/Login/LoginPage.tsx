import { ButtonSquare } from "components/Button";
import { PageContainer } from "components/Layout";
import { LogoForm } from "components/Logo";
import { Pages } from "configuration/constants";
import { genericMessages, loginMessages } from "configuration/messages";
import useLoginForm from "hooks/useLoginForm";
import { FormError } from "interfaces/configuration";
import React, { ChangeEvent, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Login } from "services/securitizeApi/accountService";
import { validateEmail } from "tools/formatter";
import { saveItem } from "tools/storage";

/**
 * Pantalla de login
 */
const LoginPage = () => {
  /** Navegacion */
  const navigate = useNavigate();
  const { formValues, handleChange, formErrors, submitErrors, handleErrors } =
    useLoginForm();

  const handleValidate = (): FormError | undefined => {
    let errorsNew: FormError = {};

    if (!(formValues.password && formValues.password.length > 1)) {
      errorsNew = {
        ...errorsNew,
        password: "La password es obligatoria",
      };
    } else {
      handleErrors("password", "");
    }

    if (!(formValues.username && formValues.username.length > 1)) {
      errorsNew = {
        ...errorsNew,
        username: "El Usuario es obligatorio",
      };
    } else {
      handleErrors("username", "");
    }
    if (errorsNew && Object.keys(errorsNew).length > 0) {
      //submitErrors(errorsNew);
      return errorsNew;
    } else {
      return undefined;
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const errors = handleValidate();
    if (errors) {
      submitErrors(errors);
    } else {
      Login(formValues)
        .then(async (response) => {
          saveItem("@accessToken", response.token);
          navigate(Pages.HOMEPAGE);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const handleEnable = () => {
    const errors = handleValidate();
  };

  return (
    <PageContainer>
      <div className="Form-Container-Public container">
        <div style={{ width: 384, marginTop: 40 }}>
          <label className="Form-Container-Subtitle" style={{ marginTop: 8 }}>
            {"logg"}
          </label>
          <h1 className="Form-Container-Header">{loginMessages.title}</h1>
          <div
            className="Form-Container-Box"
            style={{ margin: "40px 0px 40px 0px" }}>
            <div
              className={`Form-Field-Container ${
                formErrors?.username ? "error" : ""
              }`}
              style={{ marginBottom: 24 }}>
              <input
                id="inpUsername"
                placeholder="|"
                value={formValues.username}
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  handleChange("username", event.target.value);
                  handleEnable();
                  // cleanError('formFullName');
                }}
              />
              <label htmlFor="inpUsername">
                {loginMessages.form.username.title}
              </label>
            </div>
            <div
              className={`Form-Field-Container ${
                formErrors?.password ? "error" : ""
              }`}>
              <input
                type="password"
                id="inpPassword"
                placeholder="|"
                value={formValues.password}
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  handleChange("password", event.target.value);
                  handleEnable();
                  // cleanError('formFullName');
                }}
              />
              <label htmlFor="inpPassword">
                {loginMessages.form.password.title}
              </label>
            </div>
          </div>
          <ButtonSquare content={genericMessages.next} onPress={handleSubmit} />
        </div>
      </div>
    </PageContainer>
  );
};

export default LoginPage;
