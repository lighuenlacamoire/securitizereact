import { ButtonSquare } from "components/Button";
import { PageContainer } from "components/Layout";
import { Pages } from "configuration/constants";
import {
  genericMessages,
  homeMessages,
  loginMessages,
} from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { TransactionDTO } from "interfaces/models";
import { AppError } from "interfaces/services";
import React, { ChangeEvent, useEffect, useState } from "react";
import { GetAllTransactions } from "services/securitizeApi/addressService";
import { getItemAs, saveItem } from "tools/storage";
import { JsxElement } from "typescript";

const HomePage = () => {
  const [newWallet, setNewWallet] = useState<string>("");
  const [list, setList] = useState<any[]>();
  const handleSubmit = () => {
    const wallets = getItemAs<string[]>("wallets") || [];
    if (newWallet) {
      const idx = wallets.findIndex((item) => item === newWallet);
      if (idx !== -1) {
        //wallets.splice(idx, 1);
      } else {
        const news = [...wallets, newWallet];
        saveItem("wallets", news);
      }
    }
    setNewWallet("");
    checkWallets();
  };
  const checkWallets = () => {
    const wallets = getItemAs<string[]>("wallets") || [];
    const elements: JSX.Element[] = [];

    wallets.forEach(async (wallet, index) => {
      //const transactions = await GetAllTransactions(wallet);

      elements.push(renderItem(wallet, index, []));
    });

    setList(elements);
  };
  const renderItem = (
    item: string,
    index: number,
    transactions: TransactionDTO[],
  ) => {
    return (
      <div
        key={`${index}${item}`}
        style={{
          flex: 1,
          display: "flex",
          flexDirection: "row",
          backgroundColor: "gray",
          border: "1px solid blue",
          borderRadius: 8,
          padding: 8,
        }}>
        <div style={{ flex: 1 }}>{`Wallet ${index + 1}`}</div>
        <div style={{ flex: 1 }}>{item}</div>
      </div>
    );
  };

  useEffect(() => {
    checkWallets();
  }, []);
  return (
    <PageContainer>
      <div className="Form-Container-Public container">
        <div style={{ marginTop: 40 }}>
          <label className="Form-Container-Subtitle" style={{ marginTop: 8 }}>
            {"Add wallets to check"}
          </label>
          <h1 className="Form-Container-Header">{homeMessages.title}</h1>
          <div
            className="Form-Container-Box"
            style={{ margin: "40px 0px 40px 0px" }}>
            <div
              className={`Form-Field-Container `}
              style={{ marginBottom: 24 }}>
              <input
                id="inpWallet"
                placeholder="|"
                value={newWallet}
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  setNewWallet(event.target.value);
                }}
              />
              <label htmlFor="inpWallet">
                {homeMessages.form.wallet.title}
              </label>
            </div>
            <ButtonSquare
              content={genericMessages.add}
              onPress={handleSubmit}
            />
          </div>
        </div>
        <div>{list}</div>
      </div>
    </PageContainer>
  );
};
export default HomePage;
