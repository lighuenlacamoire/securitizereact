/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useEffect, useState } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import { Main } from "components/Layout";
import { RootRouter } from "routers";
import { Pages } from "configuration/constants";

const App = () => {
  return (
    <Main>
      <RootRouter />
    </Main>
  );
};

export default App;
