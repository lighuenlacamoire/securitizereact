# Dockerfile
FROM node:18-alpine AS base

FROM base AS builder

WORKDIR /app

COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./

RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
  # Allow install without lockfile, so example works even without Node.js installed locally
  else echo "Warning: Lockfile not found. It is recommended to commit lockfiles to version control." && yarn install; \
  fi

COPY . .

ARG ENV_VARIABLE
ENV ENV_VARIABLE=${ENV_VARIABLE}

RUN \
  if [ -f yarn.lock ]; then yarn build; \
  elif [ -f package-lock.json ]; then npm run build; \
  elif [ -f pnpm-lock.yaml ]; then pnpm build; \
  else yarn build; \
  fi

FROM base AS runner

WORKDIR /app


COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/build .
COPY --from=builder /app/public ./public

ARG ENV_VARIABLE
ENV ENV_VARIABLE=${ENV_VARIABLE}

ENV NEXT_TELEMETRY_DISABLED 1

# Install `serve` to run the application.
RUN yarn global add serve
ENV PORT 5000

EXPOSE 5000
CMD \
  if [ -f yarn.lock ]; then yarn up; \
  elif [ -f package-lock.json ]; then npm run up; \
  elif [ -f pnpm-lock.yaml ]; then pnpm up; \
  else yarn up; \
  fi